import React from 'react';
import {  Text, View } from 'react-native';
import {Header, TodoInput, TodoList} from './src/components';

const data = [
  {key: 'a', item: 'Buy Apples'},
  {key: 'b', item: 'Get dog Food'},
  {key: 'c', item: 'Get cat fooooood'},
  {key: 'd', item: 'Get SOMETHING'},
];

export default class App extends React.Component {

  state ={
    inputValue: '',
    todoData: data
  }

  onPressAdd = event =>{
    if(this.state.inputValue === '') 
      return;
    this.setState({
      todoData: [
        ...this.state.todoData,
        {
          key: `_${this.state.todoData.length}_${this.state.inputValue.length}`,  // Unique String ID
          item: this.state.inputValue
        }
      ],
      inputValue: ''
    });
  }

  onChangeTodoInput = event => {
    this,this.setState({
      inputValue: event
    })
  }

  render() {
    return (
      <View>
        {/* Header Title */}
        <Header title="Demo Todo App"/>
        {/* Input + Lable + Button Componenet */}
        <TodoInput 
          value={this.state.inputValue} 
          onPressAdd={this.onPressAdd} 
          onChangeTodoInput={this.onChangeTodoInput}
          />
          {/* Touchable List Items */}
        <TodoList data={this.state.todoData} />
      </View>
    );
  }
}

