# Pull base image.
FROM ubuntu:16.04

LABEL Description="Node LTS with yarn and react-native"
# Define commonly used JAVA_HOME variable
# ENV JAVA_HOME /usr/lib/jvm/java-8-oracle
# ENV ANDROID_SDK_VERSION r24.4.1
# ENV ANDROID_BUILD_TOOLS_VERSION build-tools-27.0.3,build-tools-23.0.3,build-tools-23.0.2,build-tools-23.0.1

# ENV ANDROID_SDK_FILENAME android-sdk_${ANDROID_SDK_VERSION}-linux.tgz
# ENV ANDROID_SDK_URL http://dl.google.com/android/${ANDROID_SDK_FILENAME}
# ENV ANDROID_API_LEVELS android-23
# ENV ANDROID_EXTRA_COMPONENTS extra-android-m2repository,extra-google-m2repository,extra-android-support,extra-google-google_play_services
# ENV ANDROID_HOME /opt/android-sdk-linux
# ENV PATH ${PATH}:${ANDROID_HOME}/tools:${ANDROID_HOME}/platform-tools
# ENV GRADLE_VERSION 4.4
# ENV GRADLE_HOME /usr/lib/gradle
# ENV PATH $PATH:$GRADLE_HOME/bin
# ENV NODE_VERSION 6.9.1
# ENV PATH ${PATH}:/opt/node/bin
# ENV LANG en_US.UTF-8
# ENV BUILD_PACKAGES git yarn nodejs build-essential imagemagick librsvg2-bin ruby ruby-dev wget libcurl4-openssl-dev

# Repo for Yarn
RUN apt-key adv --fetch-keys http://dl.yarnpkg.com/debian/pubkey.gpg
RUN echo "deb http://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list

# Install base software packages
RUN apt-get update && \
    apt-get install software-properties-common \
    python-software-properties \
    wget \
    curl \
    git \
    unzip -y \
    yarn && \
    # echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | debconf-set-selections && \
    # add-apt-repository -y ppa:webupd8team/java && \
    # apt-get update && \
    # apt-get install -y oracle-java8-installer && \
    # rm -rf /var/lib/apt/lists/* && \
    # rm -rf /var/cache/oracle-jdk8-installer \
    # dpkg --add-architecture i386 && \
    # apt-get update -y && \
    # apt-get install -y libc6:i386 libncurses5:i386 libstdc++6:i386 lib32z1 && \
    # rm -rf /var/lib/apt/lists/* && \
    # apt-get autoremove -y && \
    # cd /opt && \
    # wget -q ${ANDROID_SDK_URL} && \
    # tar -xzf ${ANDROID_SDK_FILENAME} && \
    # rm ${ANDROID_SDK_FILENAME} && \
    # echo y | android update sdk --no-ui -a --filter tools,platform-tools,${ANDROID_API_LEVELS},${ANDROID_BUILD_TOOLS_VERSION} && \
    # echo y | android update sdk --no-ui --all --filter "${ANDROID_EXTRA_COMPONENTS}" \
    # cd /usr/lib \
    # && curl -fl https://downloads.gradle.org/distributions/gradle-${GRADLE_VERSION}-all.zip -o gradle-bin.zip \
    # && unzip "gradle-bin.zip" \
    # && ln -s "/usr/lib/gradle-${GRADLE_VERSION}/bin/gradle" /usr/bin/gradle \
    # && rm "gradle-bin.zip" \
    # cd && \
    # wget -q http://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz && \
    # tar -xzf node-v${NODE_VERSION}-linux-x64.tar.gz && \
    # mv node-v${NODE_VERSION}-linux-x64 /opt/node && \
    # rm node-v${NODE_VERSION}-linux-x64.tar.gz \
    # npm install react-native-cli --global \
    # mkdir -p /etc/udev/rules.d/ && cd /etc/udev/rules.d/ && wget https://raw.githubusercontent.com/M0Rf30/android-udev-rules/master/51-android.rules \
    # echo "Installing Additional Libraries" \
	# && rm -rf /var/lib/gems \
	# && apt-get update && apt-get install $BUILD_PACKAGES -qqy --no-install-recommends \
    # echo "Installing Fastlane 2.61.0" \
	# && gem install fastlane badge -N \
	# && gem cleanup \
    # mkdir /workspace \
    apt-get clean

# ——————————
# Install Java.
# ——————————

RUN \
  echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | debconf-set-selections && \
  add-apt-repository -y ppa:webupd8team/java && \
  apt-get update && \
  apt-get install -y oracle-java8-installer && \
  rm -rf /var/lib/apt/lists/* && \
  rm -rf /var/cache/oracle-jdk8-installer

# Define commonly used JAVA_HOME variable
ENV JAVA_HOME /usr/lib/jvm/java-8-oracle

# ——————————
# Installs i386 architecture required for running 32 bit Android tools
# ——————————

RUN dpkg --add-architecture i386 && \
    apt-get update -y && \
    apt-get install -y libc6:i386 libncurses5:i386 libstdc++6:i386 lib32z1 && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get autoremove -y && \
    apt-get clean

# ——————————
# Installs Android SDK
# ——————————

ENV ANDROID_SDK_VERSION r24.4.1
ENV ANDROID_BUILD_TOOLS_VERSION build-tools-27.0.3,build-tools-23.0.3,build-tools-23.0.2,build-tools-23.0.1

ENV ANDROID_SDK_FILENAME android-sdk_${ANDROID_SDK_VERSION}-linux.tgz
ENV ANDROID_SDK_URL http://dl.google.com/android/${ANDROID_SDK_FILENAME}
ENV ANDROID_API_LEVELS android-23
ENV ANDROID_EXTRA_COMPONENTS extra-android-m2repository,extra-google-m2repository,extra-android-support,extra-google-google_play_services
ENV ANDROID_HOME /opt/android-sdk-linux
ENV PATH ${PATH}:${ANDROID_HOME}/tools:${ANDROID_HOME}/platform-tools
RUN cd /opt && \
    wget -q ${ANDROID_SDK_URL} && \
    tar -xzf ${ANDROID_SDK_FILENAME} && \
    rm ${ANDROID_SDK_FILENAME} && \
    echo y | android update sdk --no-ui -a --filter tools,platform-tools,${ANDROID_API_LEVELS},${ANDROID_BUILD_TOOLS_VERSION} && \
    echo y | android update sdk --no-ui --all --filter "${ANDROID_EXTRA_COMPONENTS}"


# ——————————
# Installs Gradle
# ——————————

# Gradle
ENV GRADLE_VERSION 4.4

RUN cd /usr/lib \
 && curl -fl https://downloads.gradle.org/distributions/gradle-${GRADLE_VERSION}-all.zip -o gradle-bin.zip \
 && unzip "gradle-bin.zip" \
 && ln -s "/usr/lib/gradle-${GRADLE_VERSION}/bin/gradle" /usr/bin/gradle \
 && rm "gradle-bin.zip"

# Set Appropriate Environmental Variables
ENV GRADLE_HOME /usr/lib/gradle
ENV PATH $PATH:$GRADLE_HOME/bin

# ——————————
# Install Node
# ——————————
ENV NODE_VERSION 6.9.1
RUN cd && \
    wget -q http://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz && \
    tar -xzf node-v${NODE_VERSION}-linux-x64.tar.gz && \
    mv node-v${NODE_VERSION}-linux-x64 /opt/node && \
    rm node-v${NODE_VERSION}-linux-x64.tar.gz
ENV PATH ${PATH}:/opt/node/bin

# ——————————
# Install React-Native package
# ——————————
RUN npm install react-native-cli --global

ENV LANG en_US.UTF-8

# ——————————
# Install udev rules for most android devices
# ——————————
RUN mkdir -p /etc/udev/rules.d/ && cd /etc/udev/rules.d/ && wget https://raw.githubusercontent.com/M0Rf30/android-udev-rules/master/51-android.rules

# ——————————
# Install other dependencies like GEM for Fastlanes
# ——————————
ENV BUILD_PACKAGES git yarn nodejs build-essential imagemagick librsvg2-bin ruby ruby-dev wget libcurl4-openssl-dev
RUN echo "Installing Additional Libraries" \
	 && rm -rf /var/lib/gems \
	 && apt-get update && apt-get install $BUILD_PACKAGES -qqy --no-install-recommends

RUN echo "Installing Fastlane 2.61.0" \
	&& gem install fastlane badge -N \
	&& gem cleanup


# Get latest NPM
RUN npm i npm@latest -g

# This creates the working directory for an android project and Node.
# Has the android project and the NPM package definition 
# + project
#   + android
# - package.json
WORKDIR project
COPY android /project/android
COPY package.json /project
RUN npm install \
    && cd android \
    && fastlane beta