import React from 'react';
import {View, Text,StyleSheet} from 'react-native';


const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      marginTop: 15,
    },
    headerContent: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#003465'
    }
  });

const Header = props => {
    return (
        <View style={styles.container}>
            <Text style={styles.headerContent}>{props.title}</Text>
        </View>
    );
};

export default Header;