import React from 'react';
import { View, TextInput, Text, TouchableOpacity, Button, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
    },
    input: {
        width: 270,
        marginRight: 10,
        fontSize: 20
    },
    label: {
        marginTop: 20,
        fontWeight: 'bold',
        fontSize: 18,
        paddingLeft: 8
    },
    button: {
        alignItems: 'center',
        backgroundColor: '#0056',
        width: 60,
        height: 50,
        paddingTop: 15,
        marginTop: 10
    },
    btnText: {
        color: '#fff'
    }
});

const TodoInput = props => {
    return (
        <View style={styles.container}>
            <Text style={styles.label} >Todo: </Text>
            <TextInput placeholder="Add a todo....." style={styles.input} onChangeText={props.onChangeTodoInput} value={props.value} />
            {/* <Button title="Add" onPress={props.onPressAdd} /> */}
            <TouchableOpacity style={styles.button } onPress={props.onPressAdd} >
                <Text style={styles.btnText}>Add</Text>
            </TouchableOpacity>
        </View>
    );
};

export default TodoInput;