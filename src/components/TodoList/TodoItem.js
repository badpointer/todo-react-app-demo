import React, {PureComponent} from 'react';
import {TouchableOpacity, View, Text ,StyleSheet} from 'react-native';


const styles = StyleSheet.create({
    viewContainer: {
      alignItems: 'center',
      backgroundColor: '#D3D3D3',
      margin: 5,
      padding: 10
    },
    itemText: {
        color: '#000000'
    }
  });

class TodoItem extends PureComponent {

    selectedItem = () => {
        alert(this.props.item);
    }
    
    render() {
        return (
            <TouchableOpacity onPress={this.selectedItem} >
                <View style={styles.viewContainer}>
                    <Text style={styles.itemText}>{this.props.item}</Text>
                </View>
            </TouchableOpacity>
        );
    }
};

export default TodoItem;