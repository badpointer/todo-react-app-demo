import React from 'react'
import {View, Text, FlatList, StyleSheet} from 'react-native';
import TodoItem from './TodoItem';



const TodoList = props => {
    return (
        <View>
            <FlatList
                data={props.data}
                renderItem={({item})=> <TodoItem {...item}/>}
            />
        </View>
    );
};

export default TodoList;